# Specyfikacja

Urządzenie jest uniwersalną płytką uruchomieniową do MCU STM32H573RIT (https://www.st.com/en/microcontrollers-microprocessors/stm32h573ri.html).

# Projekt PCB

Schemat: [doc/STM32H573_EVB_V1_0_SCH.pdf](doc/STM32H573_EVB_V1_0_SCH.pdf)

Widok 3D: [doc/STM32H573_EVB_V1_0_3D.pdf](doc/STM32H573_EVB_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

![](resource/pcb_3d.png "PCB 3D")

![](resource/pcb.png "PCB top")

# Licencja

MIT
